# OpenDrive API module for Python

This is an OpenDrive API utility implemented as a Python class. It has a simple CLI, which comes as a separate file that you are free to delete if you only intend to use this class as module for a bigger application.

## Requirements
	- requests >= 2.4.3
	- requests_toolbelt

## Usage

### Import
```python
from OpenDriveAPI import OpenDriveClient
```

### Log in
To log into OpenDrive, simply create a new instance of OpenDriveClient class with your username and password passed to &#95;&#95;init&#95;&#95;()
```python
client = OpenDriveClient(username, password)
```
### List files
To get the list of files and folders in current directory you should use __list()__ method with folder ID passed to it. Folder ID (as well as file ID) is a key used in OpenDrive to identify folders (or files respectively). You can get file or folder IDs by navigating with "list", starting with folder ID "0", which is a root folder of your OpenDrive file hierarchy. Note that __list()__ returns JSON dict in __Unicode__, which contains loads of useful and not-so-useful data. For more information consult relevant part of [OpenDrive API Documentation].
```python
json = client.list(folderID="0")
```
### Upload a file
OpenDrive API provides quite a complex mechanism of managing file uploads. This module allows you to do it by just calling __upload()__ method with target file and remote folder ID specified.
```python
client.upload(path, folderID="0")
```
### Delete a file
Files on OpenDrive can be sent to trash by __delete()__ method. You will have to specify file ID to call it, which can be acquired by using __list()__.
```python
client.delete(fileID)
```
### Get file download url
Although you can get download URL by using __list()__, sometimes it is easier to get it with __getURL()__ with file ID specified. That way you will not have to parse JSON returned by __list()__.
```python
url = ""
url = client.getURL(fileID)
```
### Get download urls of all files
Similarly to __getURL()__, you can use __getURLs()__ to get download urls for all files in a specified folder. You will need the folder ID to do that.
```python
urls = []
urls = client.getURLs(folderID)
```
Note that URLs in the array are __Unicode__ encoded

### Change access mode of a file
OpenDrive utilizes a simple mechanism of file access -- they can be private, public or hidden. To change access mode use __access2file()__.
```python
client.access2file(fileID, code)
```
You can pass one of _IS&#95;PRIVATE_, _IS&#95;PUBLIC_ or _IS&#95;HIDDEN_ as an access mode:
```python
client.access2file(fileID, OpenDriveClient.IS_PRIVATE)
```
### Change access mode of a folder
The process is basically the same to using __access2file()__, but you will need to call __access2folder()__ and specify folder ID instead.
```python
client.access2folder(folderID, code)
```
You can pass the same access modes as in __access2file()__

### Log out
The logout signal is called in &#95;&#95;del&#95;&#95;() event so that the session existence time is proportional to class instance existence time


[OpenDrive API Documentation]:http://www.opendrive.com/guide/win/OpenDrive_API_guide.pdf
from requests_toolbelt import MultipartEncoder
import requests
import os

class OpenDriveClient(object):

	IS_PRIVATE = "0"
	IS_PUBLIC = "1"
	IS_HIDDEN = "2"
	
	def __init__(self, username, password): # Log in upon class instanse initialisation
		data = {
		    "username": username,
		    "passwd": password,
		    "version": ""
		}
		response = requests.post("https://dev.opendrive.com/api/v1/session/login.json",data)
		self.sessionID = response.json().get("SessionID") # @TODO Store other information about user for future use?
		if self.sessionID is None:
			raise Exception("Failed. Server response:\n Response code:"+str(response.status_code)+"\n"+response.text)

	def __del__(self): # Log out upon class instance deletion
		data = {
			"session_id": self.sessionID
		}
		requests.post("https://dev.opendrive.com/api/v1/session/logout.json",data)

	def upload(self, path, folderID="0", force=False):

		f = open(path,"rb") # Use "rb" mode when opening file (fix for illegal tell() result in Windows when reading UNIX-style line-endings)

		# Getting file size by moving pointer to the very end of it
		f.seek(0,2)
		fileSize = f.tell() 

		# Creating new file on server
		data = {
		    "session_id": self.sessionID,
		    "folder_id": folderID,
		    "file_name": os.path.basename(path),
		    "file_size": fileSize,
		    "access_folder_id": ""
		}

		response = requests.post("https://dev.opendrive.com/api/v1/upload/create_file.json",data)
		# @TODO Implement forced file upload
		if response.status_code!=200 or (not force and response.json().get("FileExists") is not None):
			raise Exception("Failed. Server response:\n Response code:"+str(response.status_code)+"\n"+response.text)
		fileID = response.json().get("FileId")
		timestamp = response.json().get("DirUpdateTime")

		# Open file upload
		data = {
			"session_id": self.sessionID,
			"file_id": fileID,
			"file_size": fileSize,
			"access_folder_id": ""
		}
		response = requests.post("https://dev.opendrive.com/api/v1/upload/open_file_upload.json",data)
		if response.status_code!=200:
			raise Exception("Failed. Server response:\n Response code:"+str(response.status_code)+"\n"+response.text)
		tempLocation = response.json().get("TempLocation")
		# Upload file (finaly)
		data = MultipartEncoder({	# No integers. Probably strings only.
			"session_id": self.sessionID,
			"file_id": fileID,
			"temp_location": tempLocation,
			"chunk_offset": str(0), # @TODO Partial upload (upload speed controll, etc)?
			"chunk_size": str(fileSize),
			"file_data": (f.name, open(f.name,"rb")) # Somehow, we can't pass file-object here. Bug?
		})
		response = requests.post("https://dev.opendrive.com/api/v1/upload/upload_file_chunk.json", data=data, headers={"Content-Type":data.content_type})
		if response.status_code!=200:
			raise Exception("Failed. Server response:\n Response code:"+str(response.status_code)+"\n"+response.text)
		# Close file upload
		data = {
		    "session_id": self.sessionID,
		    "file_id": fileID,
		    "temp_location": tempLocation,
		    "file_size": fileSize,
		    "file_time": timestamp,
		    "access_folder_id": ""
		}
		response = requests.post("https://dev.opendrive.com/api/v1/upload/close_file_upload.json",data)
		if response.status_code!=200:
			raise Exception("Failed. Server response:\n Response code:"+str(response.status_code)+"\n"+response.text)
		# Returns id of the uploaded file
		return fileID

	def getURL(self, fileID): # Get download link by file id

		response = requests.get("https://dev.opendrive.com/api/v1/file/info.json/"+self.sessionID+"/"+fileID)
		if response.status_code!=200:
			raise Exception("Failed. Server response:\n Response code:"+str(response.status_code)+"\n"+response.text)
		downloadLink = response.json().get("DownloadLink")
		return downloadLink

	def list(self, folderID="0"):
		response = requests.get("https://dev.opendrive.com/api/v1/folder/list.json/"+self.sessionID+"/"+folderID)
		if response.status_code!=200:
			raise Exception("Failed. Server response:\n Response code:"+str(response.status_code)+"\n"+response.text)
		return response.json()

	def delete(self, fileID):
		data = {
			"session_id": self.sessionID,
			"file_id": fileID,
			"access_folder_id": ""
		}
		response = requests.post("https://dev.opendrive.com/api/v1/file/trash.json",data)
		if response.status_code!=200:
			raise Exception("Failed. Server response:\n Response code:"+str(response.status_code)+"\n"+response.text)

	def access2file(self, fileID, status):
		data = {
			"session_id": self.sessionID,
			"file_id": fileID,
			"file_ispublic": status,
			"access_folder_id": ""
		}
		response = requests.post("https://dev.opendrive.com/api/v1/file/access.json",data)
		if response.status_code!=200:
			raise Exception("Failed. Server response:\n Response code:"+str(response.status_code)+"\n"+response.text)

	def access2folder(self, folderID, status):
		data = {
			"session_id": self.sessionID,
			"file_id": fileID,
			"folder_ispublic": status
		}
		response = requests.post("https://dev.opendrive.com/api/v1/folder/setaccess.json",data)
		if response.status_code!=200:
			raise Exception("Failed. Server response:\n Response code:"+str(response.status_code)+"\n"+response.text)

	def getURLs(self, folderID):
		data = self.list(folderID)
		links = []
		for f in data["Files"]:
			links.append(f["DownloadLink"])
		return links

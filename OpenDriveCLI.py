from OpenDriveAPI import OpenDriveClient

def cmdexit():
	exit()

def cmdhelp():
	with open("help", 'r') as f:
   			print "\n"+f.read()+"\n"

def cmdupload():
	c.upload(path=raw_input("File to upload?\n"), folderID=raw_input("Folder ID?\n"))

def cmdlist():
	ls = c.list(folderID=raw_input("Type id of the folder you want to list ('0' for root folder)\n"))
	print "Files:\n\n"
	for thing in ls.get("Files",{}):
		print "Name: "+thing["Name"].encode("utf-8")+"\n"
		print "Size: "+thing["Size"].encode("utf-8")+"\n"
		print "Access: "+thing["Access"].encode("utf-8")+"\n"
		print "File ID: "+thing["FileId"].encode("utf-8")+"\n"
		print "\n"
	print "Folders:\n\n"
	for thing in ls.get("Folders",{}):
		print "Name: "+thing["Name"].encode("utf-8")+"\n"
		print "Access: "+thing["Access"].encode("utf-8")+"\n"
		print "Folder ID: "+thing["FolderID"].encode("utf-8")+"\n"
		print "\n"

def cmdgeturl(): 
	print c.getURL(fileID=raw_input("File ID?\n"))

def cmddelete():
	c.delete(fileID=raw_input("File ID?\n"))

def cmdaccess2file():
	c.access2file(fileID=raw_input("File ID?\n"), status=raw_input("Select access mode:\n 0. Private\n 1. Public\n 2. Hidden\n"))

def cmdaccess2folder():
	c.access2folder(folderID=raw_input("Folder ID?\n"), status=raw_input("Select access mode:\n 0. Private\n 1. Public\n 2. Hidden\n"))

def cmdgeturls():
	links = c.getURLs(folderID=raw_input("Folder ID?\n"))
	for link in links:
		print link.encode('utf-8')

commands = {
	'exit' : cmdexit,
	'help' : cmdhelp,
	'upload' : cmdupload,
	'list' : cmdlist,
	'geturl' : cmdgeturl,
	'geturls' : cmdgeturls, # Note: this and previous are different
	'delete' : cmddelete,
	'access2file' : cmdaccess2file,
	'access2folder' : cmdaccess2folder,
}


username = raw_input("OpenDrive username?\n")
password = raw_input("Password?\n")
c = OpenDriveClient(username,password)

command = ""
while command!="exit":
	command = raw_input("\nInput a command. Type 'help' to get the list of available commands or 'exit' to quit the app.\n")
	command = command.strip().lower()
	if command in commands:
		commands[command]()

